---
title: "Αναλυτικη Γεωμετρία και Μιγαδικοί Αριθμοί"
author: ["Costas Smaragdakis"]
type: "docs"
lastmod: 2021-07-27T14:01:20+02:00
draft: false
weight: 1002
toc: true
summary: "Fall 2021-2022"
menu:
  mem100:
    identifier: ""
    parent: "MEM-100"
    weight: 1
    name: "Αρχική"
---

Το site του μαθήματος βρίσκεται στο παρακατω σύνδεσμο

[Αναλυτική Γεωμετρία και Μιγαδικοί Αριθμοί - Τμήμα Α (2021-2022)](https://polyhedron.math.uoc.gr/2122/moodle/course/view.php?id=5)
