---
title: "Θέματα Πιθανοτήτων - Στατιστικής / Μαθηματική Χρηματοοικονομία"
author: ["Costas Smaragdakis"]
type: "docs"
lastmod: 2022-09-12T14:01:20+02:00
draft: false
weight: 1001
toc: true
summary: "Fall 2022-2023"
menu:
  theta602:
    identifier: ""
    parent: "theta602"
    weight: 1
    name: "Αρχική"
---

Το site του μαθήματος βρίσκεται στο παρακατω σύνδεσμο

[Θέματα Πιθανοτήτων - Στατιστικής / Μαθηματική Χρηματοοικονομία](https://kesmarag.github.io/mf2223/home.html)
