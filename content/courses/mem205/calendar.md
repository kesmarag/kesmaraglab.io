---
title: "Ημερολόγιο"
author: ["Costas Smaragdakis"]
lastmod: 2021-01-27T14:01:20+02:00
type: "docs"
draft: false
weight: 1002
toc: true
menu:
  mem205:
    weight: 1002
    identifier: "ημερολόγιο"
    parent: "MEM-205"
    name: "Ημερολόγιο"
---
## 13η εβδομάδα [17-05-2021 - 23-05-2021]

[1η συνάντηση (pdf)](https://kesmarag.gitlab.io/courses/mem205/files/mem205_final_lecture_part_a.pdf)
[2η συνάντηση (pdf)](https://kesmarag.gitlab.io/courses/mem205/files/mem205_final_lecture_part_b.pdf)

## 12η εβδομάδα [10-05-2021 - 16-05-2021]

[Διάλεξη 12ης εβδομάδας (βίντεο)](https://youtu.be/47RGCRXINpE)

[Διάλεξη 12ης εβδομάδας (pdf)](https://kesmarag.gitlab.io/courses/mem205/files/12th_week_theory.pdf)

- Αυτοπαλινδρομικό μοντέλο

[Βίντεο για υλοποίηση Python για συντελεστές μερικής αυτοσυσχέτισης - Έτους 2020](https://youtu.be/ho6sACmXoSI)

[Βίντεο για υλοποίηση Python για γραμμική παλινδρόμηση - Έτους 2020](https://www.youtube.com/watch?v=YQKlLIlpFHY)

## 11η εβδομάδα [19-04-2021 - 25-04-2021]

[Διάλεξη 11ης εβδομάδας (βίντεο)](https://youtu.be/4n13vW44I4s)

[Διάλεξη 11ης εβδομάδας (pdf)](https://kesmarag.gitlab.io/courses/mem205/files/11th_week_theory.pdf)

- Αυτοπαλινδρομικό μοντέλο
- Συντελεστές αυτοσυσχέτισης

## 10η εβδομάδα [12-04-2021 - 18-04-2021]

[Διάλεξη 10ης εβδομάδας (βίντεο)](https://youtu.be/KoehHKxhy4s)

[Διάλεξη 10ης εβδομάδας (pdf)](https://kesmarag.gitlab.io/courses/mem205/files/10th_week_theory.pdf)

- Γραμμικά φίλτρα και απλός κινητός μέσος
- Προσαρμογή τις εποχικότητας


## 9η εβδομάδα [05-04-2021 - 11-04-2021]

[Διάλεξη 9ης εβδομάδας (βίντεο)](https://youtu.be/PxUGtPVVUWg)

[Διάλεξη 9ης εβδομάδας (pdf)](https://kesmarag.gitlab.io/courses/mem205/files/9th_week_theory.pdf)

*Δυστυχώς υπάρχει ένα πρόβλημα με το ήχο στα πρώτα 20 λεπτά. Παρακαλώ συγχωρέστε με.*

- Παράδειγμα γραμμικής παλινδρόμησης με χρήση ψευδομεταβλητών
- Εισαγωγή στις χρονολογικές σειρές
- Μακροχρόνια τάση
- Περιγραφή της μακροχρόνιας τάσης με logistic function




## 8η εβδομάδα [29-03-2021 - 04-04-2021]

[Διάλεξη 8ης εβδομάδας (βίντεο)](https://youtu.be/12lyie0JQuY)

[Διάλεξη 8ης εβδομάδας (pdf)](https://kesmarag.gitlab.io/courses/mem205/files/8th_week_theory.pdf)

- Συντελεστής προσδιορισμού
- Δειγματική κατανομή της κλίσης της γραμμής παλιδρόμησης και διάστημα εμπιστοσύνης
- Διάστημα εμπιστοσύνης για την μέση τιμή της εξαρτημένης μεταβλητής
- Διάστημα εμπιστοσύνης για την τιμή της εξαρτημένης μεταβλητής για μεμονομένη παρατήρηση
- Πολλαπλή γραμμική παλινδρόμηση

## 7η εβδομάδα [22-03-2021 - 28-03-2021]
[Διάλεξη 7ης εβδομάδας - Μέρος α (βίντεο)](https://youtu.be/JaZxlk639RI)

[Διάλεξη 7ης εβδομάδας - Μέρος β (βίντεο)](https://youtu.be/AC3wFv4pDmI)

[Διάλεξη 7ης εβδομάδας (pdf)](https://kesmarag.gitlab.io/courses/mem205/files/7th_week_theory.pdf)


## 6η εβδομάδα [15-03-2021 - 21-03-2021]
[Διάλεξη 6ης εβδομάδας (βίντεο)](https://youtu.be/CYnxbzkQOFg)

[Διάλεξη 6ης εβδομάδας (pdf)](https://kesmarag.gitlab.io/courses/mem205/files/6th_week_theory.pdf)

- Καμπύλη Lorenz και συντελεστής του Gini
- Δειγματικές κατανομές για μέση τιμή και αναλογίες

[Εργαστήριο 6ης εβδομάδας (βιντεο)](https://youtu.be/VJjbtfc11k8)

[Εργαστήριο 6ης εβδομάδας α (ipynb)](https://colab.research.google.com/drive/1kDcoZjJsV1wM8WFkpbEVchqCqgOtccUt?usp=sharing)

[Εργαστήριο 6ης εβδομάδας β (ipynb)](https://colab.research.google.com/drive/1GZpdk69bHCGtpcht3BaBpczDYg4tq7cQ?usp=sharing)

## 5η εβδομάδα [08-03-2021 - 14-03-2021]
[Διάλεξη 5ης εβδομάδας (βίντεο)](https://youtu.be/6p0qZbPfcuo)

[Διάλεξη 5ης εβδομάδας (pdf)](https://kesmarag.gitlab.io/courses/mem205/files/5th_week_theory.pdf)

- Μέτρα ασυμμετρίας και κύρτωσης
- Περιγράφοντας στατιστικές κατανομές
- Συνάρτηση πυκνότητας πιθανότητας
- Κανονική κατανομή

## 4η εβδομάδα [01-03-2021 - 07-03-2021]
[Διάλεξη 4ης εβδομάδας (βίντεο)](https://youtu.be/_kuatpgsDBE)

[Διάλεξη 4ης εβδομάδας (pdf)](https://kesmarag.gitlab.io/courses/mem205/files/4th_week_theory.pdf)

- Μέση τιμή πληθυσμού vs Μέση τιμή δείγματος
- Διασπορά και τυπική απόκλιση
- Θεώρημα του Chebyshev
- Συντελεστής μεταβλητότητας

[Εργαστήριο 4ης εβδομάδας (βιντεο)](https://youtu.be/t_zHrTaKkv8)

[Εργαστήριο 4ης εβδομάδας (ipynb)](https://colab.research.google.com/drive/1Y4LksVtyp8vcKWFzuabUrWZClvKl-upg?usp=sharing)

## 3η εβδομάδα [22-02-2021 - 28-02-2021]

[Διάλεξη 3ης εβδομάδας (βίντεο)](https://youtu.be/5jEJCXk1GNQ)

[Διάλεξη 3ης εβδομάδας (pdf)](https://kesmarag.gitlab.io/courses/mem205/files/3rd_week_theory.pdf)

- Ακραίες παρατηρήσεις και κριτήριο 1.5*IQR
- Γράφημα Box-and-Whisker
- Γεωμετρικός μέσος
- Επικρατέστερη τιμή

[Εργαστήριο 3ης εβδομάδας (βιντεο)](https://youtu.be/SrwGbGamX44)

[Εργαστήριο 3ης εβδομάδας (ipynb)](https://drive.google.com/file/d/1gLntkKzPmPeb6NmwmfD9TBC_m6zfFKRr/view?usp=sharing)

[titanic_300.csv](https://drive.google.com/file/d/1tDBhEDLXtK4RtbtP-vTgJIFaV1g_bc3I/view?usp=sharing)

## 2η εβδομάδα [15-02-2021 - 21-02-2021]

[Διάλεξη 2ης εβδομάδας (βίντεο)](https://youtu.be/lZ8-oYPNt4M)

[Διάλεξη 2ης εβδομάδας (pdf)](https://kesmarag.gitlab.io/courses/mem205/files/2nd_week_theory.pdf)

- Οργάνωση ποσοτικών και ποιοτικών δεδομένων - Κανόνας του Sturges.
- Γραφική απεικόνιση ποσοτικών και ποιοτικών δεδομένων.
- Εισαγωγή στα περιγραφικά μέτρα.
- Μέση τιμή και σταθμισμένη μέση τιμή.
- Διάμεσος
- Ποσοστημόρια και τεταρτημόρια

## 1η εβδομάδα [08-02-2021 - 14-02-2021]

[Διάλεξη 1ης εβδομάδας (βίντεο)](https://youtu.be/IFCVbOk86tY)

[Διάλεξη 1ης εβδομάδας (pdf)](https://kesmarag.gitlab.io/courses/mem205/files/1st_week_theory.pdf)

- Ιστορία της στατιστικής.
- Δειγματικός χώρος και τυχαίες μεταβλητές.
- Στατιστικός πληθυσμός και τυχαία δείγματα.
- Εισαγωγή στην οργάνωση δεδομένων.

[Εργαστήριο 1ης εβδομάδας (βιντεο)](https://youtu.be/_W3sxZXq_F0)

[Εργαστήριο 1ης εβδομάδας (ipynb)](https://colab.research.google.com/drive/1pLWm6Yiqf9eHrvX4wbFrkzxryAlKoGYt?usp=sharing)

- Εισαγωγή στο Jupyter και στο Pandas



