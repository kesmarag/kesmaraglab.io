import numpy as np
import matplotlib.pyplot as plt
from matplotlib import animation

tmax = 10
c = 2

x = np.linspace(-4*2*np.pi,4*2*np.pi,1024)
t = np.linspace(0,tmax,30*tmax)

def f(x,t): 
    return 3*np.sin(x-c*t)/2
def g(x,t): 
    return -np.sin(x+c*t)/2

fig = plt.figure()
ax = plt.axes(xlim=(x[0], x[-1]), ylim=(-3, 3))
time_text = ax.text(0.5,0.98,'',ha='left',va='top',transform=ax.transAxes)
instance, = ax.plot([], [])

def update_data(t):
    instance.set_data(x, f(x,t)+g(x,t))
    time_format='time={0:0.1f} seconds'
    time_text.set_text(time_format.format(t))
    return instance,

anim = animation.FuncAnimation(fig, update_data, frames=t, repeat=False, interval=30., blit=False)
# anim.save('wave1.gif', fps=30)
plt.show()


