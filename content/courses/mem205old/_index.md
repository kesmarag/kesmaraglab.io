---
title: "Περιγραφική Στατιστική"
author: ["Costas Smaragdakis"]
lastmod: 2020-09-13T19:59:12+03:00
type: "docs"
draft: false
weight: 1002
toc: true
summary: "Autumn 2019-2020"
menu:
  mem205old:
    identifier: ""
    parent: "MEM-205"
    weight: 1
    name: "Αρχική"
---

Το site του μαθήματος βρίσκεται στο παρακατω σύνδεσμο

[Περιγραφική Στατιστική 2019-2020](/descriptive-statistics)
