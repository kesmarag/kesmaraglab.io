## 1η διάλεξη 03-02-2020
[1st.pdf](./lectures/1st.pdf)

- Εισαγωγή, πληροφορίες για το μάθημα.
- Ιστορία της στατιστικής.
- Δειγματικός χώρος και τυχαίες μεταβλητές.
- Στατιστικός πληθυσμός και τυχαία δείγματα.
- Εισαγωγή στην οργάνωση δεδομένων.

## 1ο εργαστήριο 05-02-2020
[1st_lab.pdf](./lectures/1st_lab.pdf)

- Γνωριμία με το περιβάλλον του Jupyter.
- Εισαγωγή στη Pandas.

## 2η διάλεξη 06-02-2020
[2nd.pdf](./lectures/2nd.pdf)

- Οργάνωση ποσοτικών και ποιοτικών δεδομένων - Κανόνας του Sturges.
- Γραφική απεικόνιση ποσοτικών και ποιοτικών δεδομένων.
- Εισαγωγή στα περιγραφικά μέτρα.
- Μέση τιμή και σταθμισμένη μέση τιμή.
- Εισαγωγικά για τη διάμεσο.

## 3η διάλεξη 10-02-2020
[3rd.pdf](./lectures/3rd.pdf)

[3rd_annotated.pdf](./lectures/3rd_annotated.pdf)

- Διάμεσος
- Ποσοστημόρια
- Τεταρτημόρια
- Ακραίες παρατηρήσεις και κριτήριο 1.5*IQR
- Γράφημα Box-and-Whisker

## 2ο εργαστήριο 12-02-2020
[2nd_lab.pdf](./lectures/2nd_lab.pdf)

- Εφαρμογή του κανόνα του Sturges
- Γραφική παρουσίαση δεδομένων


## 4η διάλεξη 13-02-2020
[4th.pdf](./lectures/4th.pdf)

[4th_annotated.pdf](./lectures/4th_annotated.pdf)

- Γεωμετρικός μέσος
- Επικρατέστερη τιμή
- Μέση τιμή πληθυσμού vs Μέση τιμή δείγματος
- Διασπορά και τυπική απόκλιση 
- Θεώρημα του Chebyshev


## 5η διάλεξη 17-02-2020
[5th_annotated.pdf](./lectures/5th_annotated.pdf)

- Συντελεστής μεταβλητότητας
- Γραμμικός μετασχηματισμός και περιγραφικά μέτρα
- Καμπύλη Lorenz και συντελεστής του Gini
- Μέτρα ασυμμετρίας (Pearson και Bowley)

## 3ο εργαστήριο 19-02-2020
[3rd_lab.pdf](./lectures/3rd_lab.pdf)

[3rd_lab.ipynb](./lectures/3rd_lab.ipynb)

- Pandas' describe() function
- Μέτρα ασυμμετρίας
- Καμπύλη Lorenz και συντελεστής του Gini

## 6η διάλεξη 20-02-2020
[6th_annotated.pdf](./lectures/6th_annotated.pdf)

- Συντελεστής ασυμμετρίας Fisher-Pearson 
- Κύρτωση
- Περιγράφοντας στατιστικές κατανομές
- Συνάρτηση πυκνότητας πιθανότητας
- Κανονική κατανομή


## 7η διάλεξη 24-02-2020
[7th_annotated.pdf](./lectures/7th_annotated.pdf)

- Συνδιασμοί και Διατάξεις
- Δειγματικές κατανομές για μέση τιμή και αναλογίες
- Διαστήματα εμπιστοσύνης για αναλογίες στο πληθυσμό

## 4o εργαστήριο 26-02-2020
[4th_lab.pdf](./lectures/4th_lab.pdf)

- Υπολογισμός του συντελεστή ασυμμετρίας Fisher-Pearson
- Υπολογισμός της κύρτωσης 
- Εφαρμογή δειγματικής κατανομής αναλογίας

## 8η διάλεξη 27-02-2020
[8th_annotated.pdf](./lectures/8th_annotated.pdf)

- Διαστηματα εμπιστοσύνης για τη μέση τιμή για γνωστή τυπική απόκλιση
- Διαστηματα εμπιστοσύνης για τη μέση τιμή για άγνωστη τυπική απόκλιση
- Σχέσεις δύο μεταβλητών

## 1η εργαστηριακή εξέταση 04-03-2020

## 9η διάλεξη 05-03-2020
[9th_annotated.pdf](./lectures/9th_annotated.pdf)

- Απλή γραμμική παλινδρόμηση
- Εκτίμηση παραμέτρων με χρήση μεθόδου ελαχίστων τετραγώνων
- Διανυσματική μορφή απλής γραμμικής παλινδρόμησης

## 10η διάλεξη 09-03-2020
[10th_annotated.pdf](./lectures/10th_annotated.pdf)

- Εφαρμογή διανυσματικής μορφής απλής γραμμικής παλινδρόμησης
- Τυπική απόκλιση των τυχαίων σφαλμάτων
- Συντελεστής προσδιορισμού
- Δειγματική κατανομή της κλίσης της γραμμής παλιφρόμησης και διάστημα εμπιστοσύνης
- Γενικευμένα γραμμικά μοντέλα

## 11η διάλεξη 23-03-2020
[11th_annotated.pdf](./lectures/11th_annotated.pdf)

[video](https://www.youtube.com/watch?v=4y2BQyyKHIY)

- Διάστημα εμπιστοσύνης για την μέση τιμή της εξαρτημένης μεταβλητής
- Διάστημα εμπιστοσύνης για την τιμή της εξαρτημένης μεταβλητής για μεμονομένη παρατήρηση 

## 12η διάλεξη 26-03-2020
[12th_annotated.pdf](./lectures/12th_annotated.pdf)

[video](https://youtu.be/Y8yu8avuXmY)

- Πολλαπλή γραμμική παλινδρόμηση
- Γραμμική παλινδρόμηση με χρήση ψευδομεταβλητών

## 13η διάλεξη 30-03-2020
[13th_annotated.pdf](./lectures/13th_annotated.pdf)

[video](https://youtu.be/-yarciGhutk)

- Παράδειγμα γραμμικής παλινδρόμησης με χρήση ψευδομεταβλητών
- Εισαγωγή στις χρονολογικές σειρές
- Μακροχρόνια τάση

## 6ο εργαστήριο (περιέχεται και το 5ο) 01-04-2020
[6th_lab.pdf](./lectures/6th_lab.pdf)

[startups.csv](./lectures/startups.csv)

[video](https://youtu.be/YQKlLIlpFHY)

- Εφαρμογές γραμμικής παλινδρόμησης

## 14η διάλεξη 02-04-2020
[14th_annotated.pdf](./lectures/14th_annotated.pdf)

[video](https://youtu.be/GY98IIdCCbI)

- Περιγραφή της μακροχρόνιας τάσης με logistic function
- Εφαρμογή γραμμικών φίλτρων στις χρονολογικές σειρές
- Προσαρμογή της εποχικότητας

## 15η διάλεξη 06-04-2020
[15th_annotated.pdf](./lectures/15th_annotated.pdf)

[video](https://youtu.be/fgD3zauqhrg)

- Προσαρμογή της εποχικότητας
- Παραδείγματα

## 7ο εργαστήριο 08-04-2020
[7th_lab.pdf](./lectures/7th_lab.pdf)

[video](https://youtu.be/yp29Q2kgvnA)

- Εφαρμογή της προσαρμογής της εποχικότητας

## 16η διάλεξη 09-04-2020
[16th_annotated.pdf](./lectures/16th_annotated.pdf)

[video](https://youtu.be/RfMt2UokHGU)

- Προσαρμογή της μακροχρόνιας τάσης
- Εκθετική εξομάλυνση

## 17η διάλεξη 13-04-2020
[17th_annotated.pdf](./lectures/17th_annotated.pdf)

[video](https://youtu.be/s8NfuOxqBn0)

- Συνάρτηση αυτοσυσχέτισης
- Αυτοπαλινδρομικό μοντέλο
- Συνάρτηση μερικής αυτοσυσχέτισης

## 8ο εργαστήριο 22-04-2020
[8th_lab.pdf](./lectures/8th_lab.pdf)

[min_temp.csv](./lectures/min_temp.csv)

[video](https://youtu.be/ho6sACmXoSI)

- Εφαρμογή του αυτοπαλινδρομικού μοντέλου

## 18η διάλεξη 23-04-2020
[18th_annotated.pdf](./lectures/18th_annotated.pdf)

[video](https://youtu.be/VKrqaJK9xoY)

- Υπολογισμός και χρήση των συντελεστών μερικής αυτοσυσχέτισης
- Αυτοπαλινδρομικό μοντέλο k-τάξης

## 19η διάλεξη 27-04-2020
[19th_annotated.pdf](./lectures/19th_annotated.pdf)

[video](https://youtu.be/4_Bn3RURAWs)

- Εισαγωγή στα State-Space models

## 9o εργαστήριο
[9th_lab.pdf](./lectures/9th_lab.pdf)

[video](https://youtu.be/nDRb3x0e5UE)

- Εφαρμογή ενός απλού State-Space model

## 20η διάλεξη 30-04-2020
[20th_annotated.pdf](./lectures/20th_annotated.pdf)

[video](https://youtu.be/Nu_SYDY5I-A)

- Kalman gain

## 21η διάλεξη 04-05-2020

[21th_annotated.pdf](./lectures/21th_annotated.pdf)

[video](https://youtu.be/HKLbi2YPJX8)

- Εισαγωγή στους Αριθμοδείκτες

## 22η διάλεξη 06-05-2020

[22th_annotated.pdf](./lectures/22th_annotated.pdf)

[video](https://youtu.be/arX-rjLmzyY)

- Εισαγωγή στους Αριθμοδείκτες - συνέχεια

## 23η διάλεξη 07-05-2020

[23th_annotated.pdf](./lectures/23th_annotated.pdf)

[video](https://youtu.be/I80vbbOH970)

- Επίλυση ασκήσεων του φυλλαδίου (Μέρος 1/3)

## 24η διάλεξη 11-05-2020

[24th_annotated.pdf](./lectures/24th_annotated.pdf)

[video](https://youtu.be/qSzgm_OmrJc)

- Παραδείγματα

## 25η διάλεξη 13-05-2020

[25th_annotated.pdf](./lectures/25th_annotated.pdf)

[video](https://youtu.be/clyG8nILwMc)

- Παραδείγματα

## 26η διάλεξη 18-05-2020

[26th_annotated.pdf](./lectures/26th_annotated.pdf)

[video](https://youtu.be/YUa6x1iKvOU)

- Επίλυση ασκήσεων του φυλλαδίου (Μέρος 2/3)

## 27η διάλεξη 20-05-2020

[27th_annotated.pdf](./lectures/27th_annotated.pdf)

[video](https://youtu.be/wtw81ooc0-U)

- Επίλυση ασκήσεων του φυλλαδίου (Μέρος 3/3)
