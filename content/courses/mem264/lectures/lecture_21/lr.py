from sklearn.linear_model import LogisticRegression
import numpy as np

X = np.array([168, 176, 165, 179, 188, 178, 176])
y = np.array([  0,   1,   0,   0,   1,   1,   1])

lr_model = LogisticRegression()

lr_model.fit(np.expand_dims(X,1), y)

print(lr_model.predict([[200]]))
