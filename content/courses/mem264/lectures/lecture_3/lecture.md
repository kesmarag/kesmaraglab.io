---
title: 'MEM-264 Applied Statistics'
subtitle: 'Department of Mathematics and Applied Mathematics, University of Crete'
author: Costas Smaragdakis (kesmarag@uoc.gr)
date: 3rd Lecture - 15-02-2021
output: beamer_presentation
theme: 'kesmaragphd'
aspectratio: 169
header-includes:
 - \usepackage{fontspec}
 - \setmainfont{IBM Plex Sans}
 - \setsansfont{IBM Plex Sans}
---

## The Poisson distribution

The following condition must be satisfied to apply the Poisson distribution.

- The occurences of a phenomenon are random and independent.

### Definition

Let $X\sim \mathrm{Po}(\lambda)$ denote that the random variable $X$ obeys the Poisson
distribution with parameter $\lambda>0$. 

$$ f_X(x) = \frac{\lambda^x e^{-\lambda}}{x!},$$

where x is any non-negative integer number.

## The Poisson distribution

### Example

On average 5.2 robberies occur per day in a particular city.

1. Calculate the probability that no robberies took place in this city on a given day. 
2. Calculate the probability that on a given day the number of robberies will be 1 to 5.

\vspace{5cm}


## The normal (Gaussian) distribution
- Many things and phenomena in nature closely follow a normal distribution.
Let $X\sim \mathcal N (\mu, \sigma^2)$ denote that the random variable $X$ follows the
normal distribution with parameters $\mu$ and $\sigma^2$. 

$$ f_X(x) = \frac{1}{\sqrt{2 \pi \sigma^2}}\exp\biggl( -
\frac{(x-\mu)^2}{2\sigma^2}\biggr)$$

### Definition : Standard normal distribution

The normal distribution with $\mu=0$ and $\sigma^2 =1$ is called the **standard normal
distribution**

### Definition : z-score

$$z = z(x) = \frac{x-\mu}{\sigma}$$

The absolute value of the z-score of $x$ gives the distance of $x$ from the mean value in terms of standard deviations.


## Standard normal distribution

![alt text](./z_table.png)

## Approximation of a binomial distribution using a normal distribution

If $X\sim \mathrm{Bin}(N,p)$ and $\max\{Np, N(1-p)\}>5$ then 
the normal distribution $\mathcal N(np, np(1-p))$ can be used as an approximation
of $\mathrm{Bin}(N,p)$.

![](./normal_approximation.png)

## Expectations and variances (binomial, poisson, normal)

- $X \sim \mathrm{Bin}(N,p)$
$$\mathbb E(X) = Np,\quad \mathrm{Var}(X) = Np(1-p)$$

- $X \sim \mathrm{Po}(\lambda)$
$$\mathbb E(X) = \lambda,\quad \mathrm{Var}(X) = \lambda$$

- $X \sim \mathcal N(\mu, \sigma^2)$
$$\mathbb E(X) = \mu, \quad \mathrm{Var}(X) = \sigma^2 $$

## Parametric space $\Theta$

$$ f_X(x) \Leftrightarrow f_X(x;\theta),\ \text{where } \ \theta \in \Theta \subseteq \mathbb R^d$$

- For $\theta_1, \theta_2 \in \Theta$ with $\theta_1 \neq \theta_2$ the distributions
	coresponding to $\theta_1$ and $\theta_2$ are different.

### Examples

- Poisson distribution : $\theta = \lambda \subseteq \mathbb R$

$$ f_X(x; \theta) = \frac{\theta^x e^{-\theta}}{x!},$$

- Normal distribution : $\theta = (\theta_1, \theta_2)^T = (\mu, \sigma)^T \subseteq \mathbb R^2$

$$ f_X(x;\theta) = \frac{1}{\sqrt{2 \pi \theta_2^2}}\exp\biggl( -
\frac{(x-\theta_1)^2}{2\theta_2^2}\biggr)$$


## Statistics

- Let $X_1,\dots,X_n$ be a sample and $\mathbf X = (X_1,\dots,X_n)^T$ is a
	multidimensional random variable with elements the random variables of the sample. 
- We refer to the sample space of $\mathbf X$ as $\mathcal X$.

### Definition
Any function of a statistical sample is called a statistic.

$$ t: \mathbf x \in \mathcal X \rightarrow t(\mathbf x) $$

where $\mathbf x = (x_1,\dots,x_n)^T$ any realization of the $\mathbf X$.

\vspace{1cm}

$$ T = t(\mathbf X) \in \mathcal T \ \text{ is a random variable} $$

## The sample mean

### Definition
Let $X_1,\dots, X_n$ be an i.i.d sample. The sample mean $\bar X_n$ is defined as follows
$$ \bar X_n = \frac{1}{n} \sum_{j=1}^n X_j $$

- $\bar X_n$ is of course a random variable.
- We denote as $\bar x_n$ any realization of the sample mean.

### Expectation of the sample mean
$$\mathbb E(\bar X_n) = \mathbb E(X_1)$$

## The sample mean

### Variance of the sample mean

$$\mathrm{Var}(\bar X_n) = \frac{1}{n}\mathrm{Var}(X_1)$$

\vspace{2cm}

- According to the central limit theorem, if $n>>1$ then
$$ \bar X_n \rightarrow W \sim \mathcal N\biggl(\mathbb E(X_1), \frac{1}{n}\mathrm{Var}(X_1)\biggr)$$

## The sample mean

### Example : $X_1,\dots, X_n$ i.i.d, $X_1\sim \mathrm{Bin}(10, 0.2)$
\vspace{2cm}
### Example : $X_1,\dots, X_n$ i.i.d, $X_1\sim \mathrm{Po}(2.5)$
\vspace{2cm}
### Example : $X_1,\dots, X_n$ i.i.d, $X_1\sim \mathcal N(1, 3)$
\vspace{2cm}
