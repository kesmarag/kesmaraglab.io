---
title: 'MEM-264 Applied Statistics'
subtitle: 'Department of Mathematics and Applied Mathematics, University of Crete'
author: Costas Smaragdakis (kesmarag@uoc.gr)
date: 4th Lecture - 19-02-2021
output: beamer_presentation
theme: 'kesmaragphd'
aspectratio: 169
header-includes:
 - \usepackage{fontspec}
 - \setmainfont{IBM Plex Sans}
 - \setsansfont{IBM Plex Sans}
---


## The sample variance (δειγματική διασπορά)

###  Definition

Let $X_1,\dots,X_n$ be an i.i.d sample. The sample variance $S_n^2$ is defined as

$$S_n^2 = \frac{1}{n-1}\sum_{j=1}^n(X_j - \bar X_n)^2.$$
    
- $S_n^2$ is a random variable.
- We denote as $s_n^2$ a realization of the sample variance.

###  Expectation of the sample variance

$$\mathbb E(S_n^2) = \mathrm{Var}(X_1).$$

\vspace{1cm}


## The sample variance (δειγματική διασπορά)


## Estimators (Εκτιμήτριες)

### Definition 
A statistic $t$ is called **an estimator** of a function $g$ of the model parameters $\theta$
if it is used to estimate $g(\theta)$. 

### Definition
An estimator $t$ is called **unbiased (αμερόληπτη)** iff
$$ \mathbb E(t(\mathbf X)) = g(\theta) $$

## Testing hypotheses (ελέγχοντας στατιστικές υποθέσεις)

**Statistical model :** $f_X(x;\theta), \ \theta \in \Theta \subseteq \mathbb R^d$.

-   A hypothesis is a statement about an unknown state of the nature.

$$\Theta = \Theta_0 \cup \Theta_1,\quad \Theta_0 \cap \Theta_1 = \emptyset .$$

-   **Null hypothesis (μηδενική υπόθεση)**

$$H_0 : \theta \in \Theta_0$$

The null hypothesis is going to be the trivial statement of the problem.

-   **Alternative hypothesis (εναλλακτική υπόθεση)**

$$H_1 : \theta \in \Theta_1$$


## Simple and compositive hypotheses

A hypothesis is called **a simple hypothesis** if it specifies the statistical distribution without any uncertainty. 
Otherwise the hypothesis is called **a composite hypothesis**.

## Testing hypotheses for $\Theta \subseteq \mathbb R$

###  One-sided hypotheses

Let $\Theta = \mathbb R$,
$H_0$ and $H_1$ are one-sided hypotheses iff we have one of the following:

$$H_0 : \theta \geq \theta_0 \quad \text{vs} \quad H_1 : \theta < \theta_0$$

$$H_0 : \theta > \theta_0 \quad \text{vs} \quad H_1 : \theta \leq \theta_0$$

$$H_0 : \theta \leq \theta_0 \quad \text{vs} \quad H_1 : \theta > \theta_0$$

$$H_0 : \theta < \theta_0 \quad \text{vs} \quad H_1 : \theta \geq \theta_0$$

where $\theta_0 \in \mathbb R$.


## Testing hypotheses for $\Theta \subseteq \mathbb R$

###  Two-sided hypothesis

Let $\Theta = \mathbb R$,
We call $H_1$ two-sided hypothesis iff:
$$H_0 : \theta = \theta_0 \quad \text{vs} \quad H_1 : \theta \neq \theta_0,$$
where $\theta_0 \in \mathbb R$.


## Test statistics

###  Definition

A **test statistics** is a function that measures the inconsistency between the data and the null hypothesis.
$$\text{Random variable : } T = t(\mathbf X), \quad \mathbf X = (X_1,\dots,X_n)^T$$
$$\text{Realization : } t_{obs} = t(\mathbf x),\ \text{where } \mathbf x = (x_1,\dots,x_n)^T \ \text{is a realization of } \mathbf X$$

###  Example

Let $X_1, \dots, X_{100}$ be an i.i.d sample, and $X_1 \sim \mathcal N (\theta,1),\ \theta \in \mathbb R$
$$H_0 : \theta = 1 \quad \text{vs} \quad H_1 : \theta \neq 1$$ 
\vspace{3cm}

## Test statistics

## Test statistics

###  Definition : p-value

To any realization $t_{obs}$ of the test statistics we associate a p-value.
In the general case, the p-value is the probability of $|t(\mathbf X)|$ lying at and beyond $|t_{obs}|$.

|	p-value               | amount of evidence            |
|-----------------------|-------------------------------|
| p-value < 0.01        | strong evidence against $H_0$ |
| 0.01 < p-value < 0.05 | evidence against $H_0$        |
| 0.05 < p-value < 0.12 | weak evedence against $H_0$   |
| p-value > 0.12        | no evidence against $H_0$     |


\vspace{3cm}

## Test statistics

## Exercise
Let $X$ be $\mathrm{Bin}(100,\theta),\ \theta \in [0.5,1)$ distributed.
$$H_0 : \theta = 0.5 \quad \text{vs} \quad H_1 : \theta \in (0.5,1)$$ 
Calculate the p-value if the observed value (a single realization) of $X$ is $x=95$.
