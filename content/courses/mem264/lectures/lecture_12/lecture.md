---
title: 'MEM-264 Applied Statistics'
subtitle: 'Department of Mathematics and Applied Mathematics, University of Crete'
author: Costas Smaragdakis (kesmarag@uoc.gr)
date: 12th Lecture - 19-03-2021
output: beamer_presentation
theme: 'kesmaragphd'
aspectratio: 169
header-includes:
 - \usepackage{fontspec}
 - \setmainfont{IBM Plex Sans}
 - \setsansfont{IBM Plex Sans}
---

## Two-sided hypotheses
- $H_0 : \theta \in \Theta_0 = [\theta_1, \theta_2] \quad \text{vs} \quad H_1 : \theta \in \Theta_1 = \Theta - \Theta_0$
- If we have a distribution that is MLR with respect to $t(X)$, we can expect tests of the following form:
$$\phi(x) = \begin{cases} 1, & \text{if} \quad t(x) < t_1 \ \text{or} \ t(x) > t_2 \\
                   \gamma(x), & \text{if} \quad t(x) \in \{t_1, t_2\} \\
									         0, & \text{if} \quad t_1 < t(x) < t_2
							\end{cases}$$

## Two-sided hypotheses
When we study two-sided hypotheses, We need the concept of the unbiasedness. 

### Definition : Unbiased test functions
A test function $\phi$ is called unbiased of size of $\alpha$ if
$$\sup_{\theta \in \Theta_0} \mathbb E_\theta\{\phi(X)\}\leq \alpha$$
and 
$$\mathbb E_\theta\{\phi(X)\} \geq \alpha \ \text{for all} \ \theta \in \Theta_1$$

- An unbiased test ensures that the probability of rejecting the null hypothesis is higher
	when $H_0$ is false that when it is true !!
- **A test which is UMP amongst the class of all unbiased tests is called uniformly most powerful unbiased (UMPU) test.**
- Unbiasedness is not an optimality criterion.
- It is simply used to reduce the complexity of the problem.

## UMPU tests for one-parameter exponential families
We said that a random variable $X$ belongs to an one-dimensional exponential family iff
its probability density (or mass) function is of the following form:
$$f(x;\theta) = c(\theta) h(x) \exp\{\theta t(x)\}$$
The $T=t(X)$ is called the natural statistics of $X$.

- We will restrict our analysis to continuous random variables $X$ to avoid the need for
	randomized test functions.


## UMPU tests for one-parameter exponential families

### Theorem
Let $\phi$ be any test function. There exist a unique two-sided test function $\phi_0$
which is a function of $T=t(X)$ such that
$$\mathbb E_{\theta_j} \phi_0(X) = \mathbb E_{\theta_j} \phi(X),\ j=1,2$$
and
$$\mathbb E_\theta \phi_0 (X) - \mathbb E_\theta \phi(X) =
\begin{cases}
\leq 0, & \theta \in (\theta_1,\theta_2)\\
\geq 0, & \theta < \theta_1 \ \text{or} \theta > \theta_2
\end{cases}$$

### Colollary
Given $\alpha>0$, there exists a UMPU test of size $\alpha$, that is of two-sided form in $t(X)$.
\vspace{3cm}

## Testing a point null hypothesis
$$ H_0 : \theta = \theta_0 \quad \text{vs} \quad H_1 : \theta \neq \theta_0$$
There exists a two-sided test function for which:
$$ \mathbb E_{\theta_0} \phi_0(X) = \alpha,\quad \text{and} \quad \frac{d}{d\theta} \mathbb E_\theta \phi_0(X)
|_{\theta=\theta_0} = 0$$

- **Such a test is UMPU**
- The derivative existence follows from the assumption of exponential family !!

## Application : $X_1,\dots,X_n$ i.i.d and $X_1\sim \mathcal N(\theta,1)$
