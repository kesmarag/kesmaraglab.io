from scipy.stats import norm
import numpy as np

np.random.seed(1)

mu1 = 1
mu2 = 1
mu3 = 1
sigma2 = 2

n1 = 1000
n2 = 1000
n3 = 1000

Y1 = norm(mu1,sigma2**0.5)
Y2 = norm(mu2,sigma2**0.5)
Y3 = norm(mu3,sigma2**0.5)

y1 = Y1.rvs(n1)
y2 = Y2.rvs(n2)
y3 = Y3.rvs(n3)

bar_Y = (sum(y1) + sum(y2) + sum(y3))/(n1+n2+n3)
print(bar_Y)

bar_Y1 = sum(y1)/n1
bar_Y2 = sum(y2)/n2
bar_Y3 = sum(y3)/n3

S2_1 = sum((y1-bar_Y1)**2)/(n1-1)
S2_2 = sum((y2-bar_Y2)**2)/(n2-1)
S2_3 = sum((y3-bar_Y3)**2)/(n3-1)

S2_p = ((n1-1)*S2_1 + (n2-1)*S2_2 + (n3-1)*S2_3)/(n1+n2+n3-3)
S2_b = (n1*(bar_Y1 - bar_Y)**2 + n2*(bar_Y2 - bar_Y)**2 + n3*(bar_Y3 - bar_Y)**2)/(3-1)

print(S2_p)
print(S2_b)

F = S2_b/S2_p

print('F-statistic =', F)

