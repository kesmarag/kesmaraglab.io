#+TITLE:     MEM-264 Applied Statistics
#+SUBTITLE:  Department of Mathematics and Applied Mathematics, University of Crete
#+AUTHOR:    Costas Smaragdakis (kesmarag@uoc.gr)
#+DATE:      2nd set : 15-04-2021
#+LATEX_COMPILER: xelatex
#+LANGUAGE: en,el
#+OPTIONS: toc:nil H:2  num:nil \n:nil @:t ::t |:t ^:t -:t f:t *:t <:t
#+LATEX_HEADER:\usepackage{fontspec}
#+LATEX_HEADER:\usepackage{graphicx}
#+LATEX_HEADER:\setmainfont{IBM Plex Sans}
#+LATEX_HEADER:\setsansfont{IBM Plex Sans}
#+LATEX_HEADER:\usepackage[a4paper,total={170mm,309mm},left=16mm,right=16mm,top=10mm,bottom=22mm]{geometry}
#+LaTeX_CLASS_OPTIONS: [professionalfont,11pt]
* Exercise 1
An investigator wishes to determine whether alcohol consumption causes a deterioration
in the performance of automobile drivers. Before the driving test, subjects
drink a glass of orange juice, which, in the case of the treatment group, is laced with
two ounces of vodka. Performance is measured by the number of errors made on a
driving simulator. A total of a \(120\) volunteer subjects are randomly assigned, in equal
numbers, to the two groups. For subjects in the treatment group, the mean number
of errors \(\bar X_{n_x}\) equals \(26.4\), and for subjects in the control group, the mean number
of errors \(\bar Y_{n_y}\) equals \(18.6\). The estimated standard error is \(s_{\bar X_{n_x} - \bar Y_{n_y}} = 2.4\).
(a) Use t-test to test the null hypothesis at the \(0.05\) level of significance.
(b) Specify the p-value for this test result.
* Άσκηση 1
Ένας ερευνητής επιθυμεί να προσδιορίσει εάν η κατανάλωση αλκοόλ προκαλεί επιδείνωση
στην απόδοση των οδηγών αυτοκινήτων. Πριν από τη δοκιμή οδήγησης, οι συμμετέχοντες
πίνουν ένα ποτήρι χυμό πορτοκαλιού, το οποίο, στην περίπτωση της ομάδας θεραπείας, περιέχει
δύο ουγγιές βότκας. Η απόδοση μετράται από τον αριθμό των σφαλμάτων που έγιναν σε ένα
προσομοιωτής οδήγησης. Ένα σύνολο \(120\) εθελοντών μοιράζονται τυχαία σε δύο ομάδες (60 στη κάθε ομάδα).
Για άτομα στην ομάδα θεραπείας, ο μέσος αριθμός
των σφαλμάτων \(\bar X_{n_x}\) ισούται με \(26.4\) ενώ για τα άτομα στην ομάδα ελέγχου, ο μέσος αριθμός
σφαλμάτων \(\bar Y_{n_y} \) ισούται με \(18.6\). Το εκτιμώμενο τυπικό σφάλμα είναι \(s_{\bar X_{n_x} - \bar Y_{n_y}} = 2.4\).
(α) Χρησιμοποιήστε το t-test για να ελέγξετε την μηδενική υπόθεση με επίπεδο σημαντικότητας \(\alpha = 0.05\).
(β) Προσδιορίστε την p-value. 
* Exercise 2
Does bread lose its vitamins when stored?
Small loaves of bread were prepared with ﬂour
that was fortiﬁed with a ﬁxed amount of vitamins.
After baking, the vitamin C content of two loaves
was measured. Another two loaves were baked
at the same time, stored for one day, and then
the vitamin C content was measured. In a similar
manner, two loaves were stored for three, ﬁve, and
seven days before measurements were taken. The
units are milligrams of vitamin C per hundred
grams of ﬂour (mg/100 g). Here are the data:
|--------------------------+-----------+------------|
| Condition                | Vitamin C | (mg/100 g) |
|--------------------------+-----------+------------|
| Immediately after baking |     47.62 |      49.79 |
| One day after baking     |     40.45 |      43.46 |
| Three days after baking  |     21.25 |      22.34 |
| Five days after baking   |     13.18 |      11.65 |
| Seven days after baking  |      8.51 |       8.13 |
Perform a one-way ANOVA for these data. Be
sure to state your hypotheses, the test statistic
with degrees of freedom, and the P-value.
* Άσκηση 2
Χάνει το ψωμί τις βιταμίνες του όταν αποθηκεύεται;
Μικρές φραντζόλες ψωμιού ετοιμάστηκαν με αλεύρι στο οποίο
συμπληρώθηκε μια σταθερή ποσότητα βιταμινών.
Μετά το ψήσιμο, μετρήθηκε η περιεκτικότητα σε βιταμίνη C στα δύο ψωμιά.
Επίσης, ακόμη δύο ψωμιά ψήθηκαν
ταυτόχρονα, αποθηκεύτηκαν για μία ημέρα και μετά
μετρήθηκε η περιεκτικότητα σε βιταμίνη C. Με παρόμοιο
τρόπο, δύο καρβέλια αποθηκεύτηκαν για τρία, πέντε, και
επτά ημέρες πριν από τις μετρήσεις.
Οι μονάδες μέτρησης είναι χιλιοστόγραμμα βιταμίνης C ανά εκατό
γραμμάρια αλευριού (mg / 100 g). Ακολουθούν τα δεδομένα: 
|----------------------------+-----------+------------|
| Condition                  | Vitamin C | (mg/100 g) |
|----------------------------+-----------+------------|
| Αμέσως μετά το ψήσιμο      |     47.62 |      49.79 |
| Μια μέρα μετά το ψήσιμο    |     40.45 |      43.46 |
| Τρεις μέρες μετά το ψήσιμο |     21.25 |      22.34 |
| Πέντε μέρες μετά το ψήσιμο |     13.18 |      11.65 |
| Επτά μέρες μετά το ψήσιμο  |      8.51 |       8.13 |
Εκτελέστε one-way ANOVA για αυτά τα δεδομένα. δηλώσετε τις υποθέσεις, τους βαθμούς
της ελευθερίας και την P-value.
* Exercise 3
Do left-handed people live shorter lives than
right-handed people?
A study of this question
examined a sample of 949 death records.
People were classiﬁed by gender
(female or male) and handedness (left or right),
and a two-way ANOVA was run with the age at death
as the response variable. The F statistics were
22.36 (handedness), 37.44 (gender), and 2.10
(interaction). The following marginal mean ages
at death (in years) were reported: 77.39 (females),
71.32 (males), 75.00 (right-handed), and 66.03
(left-handed).
For each of the F statistics given above ﬁnd the
degrees of freedom and an approximate P-value.
Summarize the results of these tests.
* Ασκηση 3
Ζουν οι αριστερόχειρες λιγότερο από
τους δεξιόχειρες;
Μια μελέτη αυτής της ερώτησης
εξέτασε δείγμα 949 αρχείων θανάτου.
Οι άνθρωποι ταξινομήθηκαν ανά φύλο
(γυναίκα ή άνδρας) και ως προς το χέρι γραφής (αριστερό ή δεξιό).
Στη συνέχεια εφαρμόστηκε το μοντέλο two-way ANOVA. Οι στατιστικές F ήταν
22.36 (επικρατέστερο χέρι), 37.44 (φύλο) και 2.10
(ΑΛΛΗΛΕΠΙΔΡΑΣΗ). Οι ακόλουθες μέσες ηλικίες θανάτου (σε χρόνια) υπολογίστηκαν: 77,39 (γυναίκες),
71.32 (άνδρες), 75.00 (δεξιόχειρες) και 66.03
(αριστερόχειρες).
Για καθένα από τις στατιστικές F που δίνονται παραπάνω, βρείτε τους
βαθμούς ελευθερίας και κατά προσέγγιση την P-value.
Συνοψίστε τα αποτελέσματα των ελέγχων. 
* Exercise 4 (programming!)
The PLANTS1 dataset gives the percent of nitrogen
in four different species of plants grown in a
laboratory. The species are Leucaena leucocephala,
Acacia saligna, Prosopis juliﬂora, and Eucalyptus
citriodora. The researchers who collected these
data were interested in commercially growing
these plants in parts of the country of Jordan
where there is very little rainfall. To examine the
effect of water, they varied the amount per day
from 50 millimeters (mm) to 650 mm in 100 mm
increments. There were nine plants per species-by-
water combination. Because the plants are to be
used primarily for animal food, with some parts
that can be consumed by people, a high nitrogen
content is very desirable.
(a) Find the means for each species-by-water
combination. 
(b) Find the standard deviations for each species-
by-water combination.
(c) Run the two-way analysis of variance.
You can find the PLANTS1 dataset [[https://www.chegg.com/homework-help/questions-and-answers/plants1-data-set-data-appendix-gives-percent-nitrogen-four-different-species-plants-grown--q11981847][here]]
* Άσκηση 4 (προγραμματισμός!)
Το σύνολο δεδομένων PLANTS1 δίνει το ποσοστό αζώτου
σε τέσσερα διαφορετικά είδη φυτών που καλλιεργούνται σε α
εργαστήριο. Τα είδη είναι τα Leucaena leucocephala,
Acacia saligna, Prosopis juli ﬂora και Eucalyptus
citriodora. Οι ερευνητές που συνέλεξαν
τα δεδομένα ενδιαφέρθηκαν για εμπορική ανάπτυξη
των φυτών σε μέρη της Ιορδανίας
όπου υπάρχει πολύ μικρή βροχόπτωση. Για να εξεταστεί η
επίδραση του νερού, καθόρισαν το ποσό ανά ημέρα
από 50 χιλιοστά (mm) έως 650 mm ανά 100 mm.
Υπήρχαν εννέα φυτά ανά ζεύγος είδους-νερού.
Επειδή αυτά τα φυτά περιλαμβάνονται κυρίως σε ζωοτροφές, και ορισμένα μέρη
μπορούν να καταναλωθούν από ανθρώπους, ένα υψηλό ποσοστό αζώτου
είναι επιθυμητό.
(α) Βρείτε της μέσες τιμές κάθε συνδυασμό.
(β) Βρείτε τις τυπικές αποκλίσεις για κάθε συνδυασμό.
(γ) Εκτελέστε two-way ANOVA.
Μπορείτε να βρείτε το σύνολο δεδομένων PLANTS1  [[https://www.chegg.com/homework-help/questions-and-answers/plants1-data-set-data-appendix-gives-percent-nitrogen-four-different-species-plants-grown--q11981847][here]] 
