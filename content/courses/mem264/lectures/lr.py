import numpy as np

X = np.array([[1,0,0],
              [1,1,1],
              [1,2,0],
              [1,1,2]])

y = np.array([0,5,5,8])

A = X.T@X

print(A)
