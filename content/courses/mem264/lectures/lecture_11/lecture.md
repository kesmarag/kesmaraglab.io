---
title: 'MEM-264 Applied Statistics'
subtitle: 'Department of Mathematics and Applied Mathematics, University of Crete'
author: Costas Smaragdakis (kesmarag@uoc.gr)
date: 11th Lecture - 16-03-2021
output: beamer_presentation
theme: 'kesmaragphd'
aspectratio: 169
header-includes:
 - \usepackage{fontspec}
 - \setmainfont{IBM Plex Sans}
 - \setsansfont{IBM Plex Sans}
---

## Applications

### 1st Example : Normal distribution with known variance 
Let $X_1,\dots,X_n$ be an i.i.d sample and $X_1 \sim \mathcal N ( \theta, \sigma^2)$ where
$\sigma^2$ is known.
$$ H_0 : \theta \leq \theta_0 \quad \text{vs} \quad H_1 : \theta > \theta_0$$
\vspace{6cm}

### 2nd Example : Normal distribution with known mean value
Let $X_1,\dots,X_n$ be an i.i.d sample and $X_1 \sim \mathcal N ( \mu, \theta), \ \theta>
0$ where
$\mu$ is known.
$$ H_0 : \theta \leq \theta_0 \quad \text{vs} \quad H_1 : \theta > \theta_0$$
\vspace{6cm}

### 3rd Example : 2 normal distributions of known variances
Let $X_1 \dots,X_n$ be an i.i.d sample and $X_1 \sim \mathcal N ( \theta, \sigma_1^2)$
and $Y_1 \dots,Y_n$ be an i.i.d sample and $Y_1 \sim \mathcal N ( \alpha \theta, \sigma_2^2)$,
where $\alpha,\sigma_1^2, \sigma_2^2$ are given.
$$ H_0 : \theta \leq \theta_0 \quad \text{vs} \quad H_1 : \theta > \theta_0$$
\vspace{6cm}
