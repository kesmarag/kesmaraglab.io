#+TITLE:     MEM-264 Applied Statistics
#+SUBTITLE:  Department of Mathematics and Applied Mathematics, University of Crete
#+AUTHOR:    Costas Smaragdakis (kesmarag@uoc.gr)
#+DATE:      18th lecture - 09-04-2021
#+LATEX_COMPILER: xelatex
#+LANGUAGE: en,el
#+OPTIONS: toc:nil H:2  num:t \n:nil @:t ::t |:t ^:t -:t f:t *:t <:t
#+LATEX_HEADER:\usepackage{fontspec}
#+LATEX_HEADER:\usepackage{graphicx}
#+LATEX_HEADER:\setmainfont{IBM Plex Sans}
#+LATEX_HEADER:\setsansfont{IBM Plex Sans}
#+BEAMER_THEME: kesmaragphd
#+LaTeX_CLASS_OPTIONS: [aspectratio=169,professionalfont,smaller]

* Lecture 18
** Two-way ANOVA
- In the one-way ANOVA, we classified populations according to a single factor (categorical variable).
- In the two-way ANOVA, we will extent the model to take into account two factors.

*** Example : Student's Grades

|------------------------+------------------+------------------|
| Department             | Male (j=1)       | Female (j=2)     |
|------------------------+------------------+------------------|
| Mathematics (i=1)      | 310 (mean: 6.38) | 400 (mean: 6.52) |
| Physics (i=2)          | 510 (mean: 6.41) | 330 (mean: 6.56) |
| Computer Science (i=3) | 190 (mean: 6.44) | 210 (mean: 6.41) |

- *With this analysis we can investigate interactions between factors!*

** Formulation of the Two-way ANOVA model
\[Y_{ijk} = \mu + \alpha_{i} + \beta_{j} + \gamma_{ij} + \epsilon_{ijk},\quad  \epsilon_{ij}\sim \mathcal N(0,\sigma^2)\]
\[\sum_{i=1}^I \alpha_i = 0, \quad \sum_{j=1}^J \beta_j = 0,\quad \sum_{i=1}^I \gamma_{ij} = 0,\quad \sum_{j=1}^{J} \gamma_{ij} = 0\]

- PAIR (i=1, j=1) : \(\{Y_{1,1,1},\dots,Y_{1,1,n_{1,1}}\}\) i.i.d sample
- PAIR (i=1, j=2) : \(\{Y_{1,2,1},\dots,Y_{1,2,n_{1,2}}\}\) i.i.d sample
\hspace{1cm} $\vdots$
- GROUP (i=I,j=J) : \(\{Y_{I,J,1},\dots,Y_{I,J,n_{I,J}}\}\) i.i.d sample
\vspace{1em}
- There are three null hypotheses (Rows, Columns, interactions)
** Formulation of the Two-way ANOVA model
*** The three null hypotheses of the model
\[H_{I0} : \alpha_1 = \alpha_2 =\dots = \alpha_I\]
\[H_{J0} : \beta_1 = \beta_2 = \dots = \beta_J\]
\[H_{IJ0} : \gamma_{11} = \dots = \gamma_{IJ}\]
** Two-way ANOVA
*** Sample mean for the estimation of the mean \(\mu_{ij}\)
\[\bar Y_{ij,n_{i,j}} = \frac{1}{n_{i,j}}\sum_{k=1}^{n_{i,j}} Y_{ijk}\]
*** Sample variance for the estimation of the variance \(\sigma^2\)
\[S_{ij,n_{i,j}}^2 = \frac{1}{n_{i,j}-1}\sum_{k=1}^{n_{i,j}}e_{ijk}^2, \quad e_{ijk} = Y_{ijk} - \bar Y_{ij,n_{i,j}}\]
*** Pooled sample variance
\[S_p^2 = \dfrac{\sum_{i,j} (n_{i,j}-1)S_{ij,n_{i,j}}^2}{\sum_{i,j} (n_{i,j}-1)}\]
** Two-way ANOVA
*** Overall sample mean
\[\bar Y = \frac{1}{\sum_{i,j} n_{i,j}} \sum_{i,j,k}  Y_{ijk} \]
*** Sample variance between groups
\[S_b^2 = \frac{\mathrm{SSM}}{\mathrm{DF}} = \frac{1}{IJ-1} \sum_{i,j} (\bar Y_{ij,n_{ij}} - \bar Y)^2 \]
** Two-way ANOVA
*In two-way ANOVA*, the terms SSM and DF can be decomposed as follows:
\vspace{1em}
\[\mathrm{SSM} = \mathrm{SSI} + \mathrm{SSJ} + \mathrm{SSIJ}\]
\[\mathrm{DF} = \mathrm{DFI} + \mathrm{DFJ} + \mathrm{DFIJ}\]
*** Two-way ANOVA table
|             | DF         | F (numerator)     | F (denominator) |
|-------------+------------+-------------------+----------------|
| Rows        | I-1        | SSI/(I-1)         |  \(S^2_p\)     |
| Columns     | J-1        | SSJ/(J-1)         |  \(S^2_p\)     |
| Interaction | (I-1)(J-1) | SSIJ/[(I-1)(J-1)] |  \(S^2_p\)     |
** Two-way ANOVA - Inference

After the analysis we will obtain three p-values

*** p-value for I (or/and J)
- If this p-value is smaller than the considered \(\alpha\), this means that the first (or/and second) factor have a statistically significant effect on the values.
*** p-value for IJ
- If this p-value is smaller than the considered \(\alpha\), this means that there is a statistically significant interaction effect beetween the factors.
** Back to the student's grades

|------------------------+------------------+------------------|
| Department             | Male (j=1)       | Female (j=2)     |
|------------------------+------------------+------------------|
| Mathematics (i=1)      | 310 (mean: 6.38) | 400 (mean: 6.52) |
| Physics (i=2)          | 510 (mean: 6.41) | 330 (mean: 6.56) |
| Computer Science (i=3) | 190 (mean: 6.44) | 210 (mean: 6.41) |
** 
