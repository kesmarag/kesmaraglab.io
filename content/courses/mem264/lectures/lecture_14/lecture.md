---
title: 'MEM-264 Applied Statistics'
subtitle: 'Department of Mathematics and Applied Mathematics, University of Crete'
author: Costas Smaragdakis (kesmarag@uoc.gr)
date: 15th Lecture - 30-03-2021
output: beamer_presentation
theme: 'kesmaragphd'
aspectratio: 169
header-includes:
 - \usepackage{fontspec}
 - \setmainfont{IBM Plex Sans}
 - \setsansfont{IBM Plex Sans}
---
## Comparing two means
Let $\{X_1,\dots,X_{n_x}\}$ and $\{Y_1,\dots,Y_{n_y}\}$ be two i.i.d samples and consider the following problem:

$$H_0 : \mu_x = \mu_y \quad \text{vs} \quad H_1 : \mu_x \neq \mu_y$$

We define the test statistics $T=\frac{\bar X_{n_x}-\bar Y_{n_y}}{\sqrt{\mathrm{Var}\{{\bar X_{n_x}-\bar Y_{n_y}}}\}}$

- t-test ($n_x+n_y-2$ degrees of freedom)

## Comparing two means

## Comparing two means


<!-- # Part II : Linear Models -->

<!-- ## Formulation of the linear model (2D case) -->

<!-- ### Simple linear regression model (Μοντέλο απλής γραμμικής παλινδρόμησης) -->
<!-- Let $\{(X_1,Y_1,E_1),\dots,(X_n,Y_n,E_n)\}$ be an i.i.d sample and the random variables $X_j,Y_j,E_j$ are connected -->
<!-- through the following relation: --> 

<!-- $$Y_j = \betta_1 + \betta_2 X_j + E_j$$ -->

<!-- where $E_j \sim \mathcal N(0,\sigma_{\epsilon}^2)$ and $\betta_1,\betta_2\in \mathbb R,\ \sigma_{\epsilon}>0$ are unknown -->
<!-- parameters. -->

<!-- - The $E_j,\ j=1,\dots,n$ are unobservable (hidden) random variables. -->

<!-- We are assuming the realizations: -->

<!-- $$\{(x_1,y_1)\dots,(x_n,y_n)\}$$ -->

<!-- - We wish to find estimations for $\betta_1,\betta_2$. -->




