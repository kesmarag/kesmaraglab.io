import numpy as np
import matplotlib.pyplot as plt
from scipy.stats import norm, uniform

n = 100
X = uniform()
E = norm(0,1)

x = 10*X.rvs(200)
e = E.rvs(200)
y = 2*x+e

plt.plot(x,y, 'o', ms=2)
plt.plot((0,10),(0,20))
plt.show()
