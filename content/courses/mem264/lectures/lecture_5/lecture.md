---
title: 'MEM-264 Applied Statistics'
subtitle: 'Department of Mathematics and Applied Mathematics, University of Crete'
author: Costas Smaragdakis (kesmarag@uoc.gr)
date: 5th Lecture - 22-02-2021
output: beamer_presentation
theme: 'kesmaragphd'
aspectratio: 169
header-includes:
 - \usepackage{fontspec}
 - \setmainfont{IBM Plex Sans}
 - \setsansfont{IBM Plex Sans}
---

## Maximum likelihood estimator (Εκτιμήτρια μέγισης πιθανοφάνειας)

\vspace{0cm}

### Definition : Likelihood function

For a realization $\mathbf x$ of a random variable $\mathbf X$ with a probability density
(or mass) function $f_{\mathbf X}(\mathbf x;\theta)$, the likelihood function $\mathcal L$ is defined by
$$\mathcal L(\theta; \mathbf x) = f_{\mathbf X}(\mathbf x; \theta)$$


### Definition : Log-likelihood function

$$ \ell (\theta;\mathbf x) = \log \mathcal L(\theta;\mathbf x) $$

### Definition : Likelihood estimator

An estimator $t$ is called the **maximum likelihood estimator** of $\theta$ if

$$ t(\mathbf x) = \arg\sup_{\theta\in \Theta} \mathcal L (\theta;\mathbf x) = \arg\sup_{\theta\in \Theta} \ell(\theta;\mathbf x) $$

- We will denote the maximum likelihood estimator as $\hat \theta_{ml}$ or simply $\hat \theta$.

## Maximum likelihood estimator (Εκτιμήτρια μέγισης πιθανοφάνειας)

### Example

Let $X_1,\dots, X_n$ be an i.i.d sample and $X_1 \sim \mathcal N(\mu, \sigma^2)$. Find the
maximum likelihood estimator of $\theta = (\mu,\sigma^2)^T$.

\vspace{6cm}

## Maximum likelihood estimator (Εκτιμήτρια μέγισης πιθανοφάνειας)

### Example

Let $X_1,\dots, X_n$ be an i.i.d sample and $X_1 \sim \mathcal U(0, \theta)$. Find the
maximum likelihood estimator of $\theta$.

\vspace{6cm}

## Sum of i.i.d random variables (binomial, poisson, normal)

### Binomial distributions
Let $X_1,\dots,X_n$ be an i.i.d sample and $X_1\sim \mathrm{Bin}(N,\theta)$

$$ \sum_{j=1}^n X_j \sim \mathrm{Bin}(nN,\theta)$$

### Normal distributions
Let $X_1,\dots,X_n$ be an i.i.d sample and $X_1\sim \mathrm{Po}(\theta)$

$$ \sum_{j=1}^n X_j \sim \mathrm{Po}(n\theta)$$

### Normal distributions
Let $X_1,\dots,X_n$ be an i.i.d sample and $X_1\sim \mathcal N(\mu, \sigma^2)$

$$ \sum_{j=1}^n X_j \sim \mathcal N(n \mu, n \sigma^2)$$

## Maximum likelihood estimator (Εκτιμήτρια μέγισης πιθανοφάνειας)

### Exercise

Let $X\sim \mathrm{Bin}(4,\theta)$ and $x = 3$ a single realization of $X$. Find the
maximum likelihood estimator of $\theta$. Repeat the estimation for the independent observations
$\mathbf x = (3,1)^T$.

\vspace{6cm}

## Student's t-distribution

Let $X_1,\dots,X_n$ be an i.i.d sample with $X_1 \sim \mathcal N(\mu, \sigma^2)$.

We define the random variable $T$ as

$$ T = \frac{\bar X_n - \mu}{S_n/\sqrt{n}}$$

The distribution of $T$ is called t-distribution with $n-1$ degrees of freedom.

## Student's t-distribution

![](./t-table.png){ height=90% }

## Example

Let $X_1,\dots,X_{4}$ be an i.i.d sample and $X_1 \sim \mathcal N(\mu,\sigma^2)$.
Accept or reject the null hypothesis.
$$ H_0 : \mu = 2 \quad \text{vs} \quad H_1 : \mu \neq 2 \quad \text{observations :}\  \mathbf x = (1.5, 2.4, 1.6, 4.5)^T$$

\vspace{5cm}

## Example
