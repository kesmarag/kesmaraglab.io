from scipy.stats import norm, f, f_oneway
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

np.random.seed(1)

y1 = [85, 88, 97, 97, 77, 99, 98, 88, 85, 78, 86, 88, 82, 77, 82, 77, 86]
y2 = [91, 92, 83, 85, 87, 84, 82, 88, 75, 76, 92, 88, 89, 83, 93, 94, 80]
y3 = [79, 78, 88, 94, 92, 85, 93, 85, 82, 81, 80, 75, 84, 97, 88, 87, 77]

I = 3

y1 = np.array(y1)
y2 = np.array(y2)
y3 = np.array(y3)



df1 = pd.DataFrame(y1)
df2 = pd.DataFrame(y2)
df3 = pd.DataFrame(y3)


plt.figure()
df1.boxplot()
plt.savefig('y1.png')
plt.figure()
df2.boxplot()
plt.savefig('y2.png')
plt.figure()
df3.boxplot()
plt.savefig('y3.png')



print(df1.describe())
print(df2.describe())
print(df3.describe())



print(np.var(y1))
print(np.var(y2))
print(np.var(y3))

n1 = len(y1)
n2 = len(y2)
n3 = len(y3)

# bar y_1
bar_y1 = sum(y1)/n1
# bar y_2
bar_y2 = sum(y2)/n2
# bar y_3
bar_y3 = sum(y3)/n3

# Overall sample mean
bar_y = (sum(y1) + sum(y2) + sum(y3))/(n1+n2+n3)

# s2_1
s2_1 = sum((y1 - bar_y1)**2)/(n1-1) 
# s2_2
s2_2 = sum((y2 - bar_y2)**2)/(n2-1) 
# s2_3
s2_3 = sum((y3 - bar_y3)**2)/(n3-1) 


s2_p = ((n1-1)*s2_1 + (n2-1)*s2_2 + (n3-1)*s2_3)/(n1+n2+n3-3)

s2_b = (n1*(bar_y1 - bar_y)**2 + n2*(bar_y2 - bar_y)**2 + n3*(bar_y3 - bar_y)**2)/(I-1)

print('s2_p =', s2_p)
print('s2_b =', s2_b)

F_obs = s2_b/s2_p

print('F-obs =', F_obs)

p_value = 1 - f.cdf(F_obs, I - 1, n1 + n2 + n3 - I)

print('p_value =', p_value)

print(f_oneway(y1, y2, y3))


