(TeX-add-style-hook
 "1st_set"
 (lambda ()
   (TeX-add-to-alist 'LaTeX-provided-class-options
                     '(("article" "professionalfont" "11pt")))
   (TeX-add-to-alist 'LaTeX-provided-package-options
                     '(("ulem" "normalem") ("geometry" "a4paper" "total={170mm,309mm}" "left=16mm" "right=16mm" "top=10mm" "bottom=22mm")))
   (add-to-list 'LaTeX-verbatim-environments-local "semiverbatim")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "href")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperref")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperimage")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperbaseurl")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "nolinkurl")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "url")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "path")
   (add-to-list 'LaTeX-verbatim-macros-with-delims-local "path")
   (TeX-run-style-hooks
    "latex2e"
    "article"
    "art11"
    "graphicx"
    "grffile"
    "longtable"
    "wrapfig"
    "rotating"
    "ulem"
    "amsmath"
    "textcomp"
    "amssymb"
    "capt-of"
    "hyperref"
    "fontspec"
    "geometry")
   (LaTeX-add-labels
    "sec:org3f9c409"
    "sec:orge10ef93"
    "sec:orgcd47180"
    "sec:org7965213"
    "sec:org6b65548"
    "sec:orgffe15c2"
    "sec:org2b8da63"
    "sec:org2d3bf30"
    "sec:orge312aef"
    "sec:org40ed65e"))
 :latex)

