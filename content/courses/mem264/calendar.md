---
title: "Ημερολόγιο"
author: ["Costas Smaragdakis"]
lastmod: 2021-01-29T15:52:02+02:00
type: "docs"
draft: false
weight: 1002
toc: true
menu:
  mem264:
    weight: 1002
    identifier: "ημερολόγιο"
    parent: "MEM-264"
    name: "Ημερολόγιο"
---
## 13η εβδομάδα (13th week) [17-05-2021 - 23-05-2021]

[1η συνάντηση (pdf)](https://kesmarag.gitlab.io/courses/mem264/files/mem264_final_lecture_part_a.pdf)
[2η συνάντηση (pdf)](https://kesmarag.gitlab.io/courses/mem264/files/mem264_final_lecture_part_b.pdf)

## 12η εβδομάδα (12th week) [10-05-2021 - 16-05-2021]

[22η διάλεξη (22th lecture) -video-](https://youtu.be/0V_Az4IP6p8)
Ειναι το β μέρος της 21ης διάλεξής. Tο pdf είναι το ίδιο της 21ης διάλεξης

## 11η εβδομάδα (11th week) [19-04-2021 - 25-04-2021]

[21η διάλεξη (21th lecture) -video-](https://youtu.be/O8rffNctF48)
[21η διάλεξη (21th lecture) -pdf-](../files/21th_lecture.pdf)

## 10η εβδομάδα (10th week) [12-04-2021 - 18-04-2021]
[19η διάλεξη (19th lecture) -video-](https://youtu.be/eb-SzFVjmic)

[19η διάλεξη (19th lecture) -pdf-](../files/19th_lecture.pdf)

Παιδιά σήμερα η διάλεξη είναι μικρότερης διάρκειας καθώς οπως θα
διαπιστώσετε από το βίντεο έχω προβληματα με τον υπολογιστή και 
φαινέται κατά τμήματα η εξέλιξη της εικόνας. Θα προσπαθησω να το διορθώσω
σύντομα. Με συγχωρείτε!

[20η διάλεξη (20th lecture) -video-](https://youtu.be/bL7ISoCkW2g)

[20η διάλεξη (20th lecture) -ipynb-](../files/two-way-anova.ipynb)

[20η διάλεξη (20th lecture) -csv-](../files/Diet.csv)


## 9η εβδομάδα (9th week) [05-04-2021 - 11-04-2021]
[17η διάλεξη (17th lecture) -video-](https://youtu.be/jSgC4JY7zAs)

[17η διάλεξη (17th lecture) -pdf-](../files/17th_lecture.pdf)

[F-stat_b.py](../files/F-stat_b.py)

[18η διάλεξη (18th lecture) -video-](https://youtu.be/nbIKmo_YGuQ)

[18η διάλεξη (18th lecture) -pdf-](../files/18th_lecture.pdf)


## 8η εβδομάδα (8th week) [29-03-2021 - 04-04-2021]
[15η διάλεξη (15th lecture) -video-](https://youtu.be/bqf5ARgfwHY)

[15η διάλεξη (15th lecture) -pdf-](../files/15th_lecture.pdf)

[two_samples.py](../files/two_samples.py)

[16η διάλεξη (16th lecture) -video-](https://youtu.be/IlW87kAGaNs)

[16η διάλεξη (16th lecture) -pdf-](../files/16th_lecture.pdf)

[F-stat.py](../files/F-stat.py)


## 7η εβδομάδα (7th week) [22-03-2021 - 28-03-2021]
[13η διάλεξη (13th lecture) -video-](https://youtu.be/WZD2OQHU-xY)

[13η διάλεξη (13th lecture) -pdf-](../files/13th_lecture.pdf)

[14η διάλεξη (14th lecture) -video-](https://youtu.be/BkEOP3n0CWs)

[14η διάλεξη (14th lecture) -pdf-](../files/14th_lecture.pdf)

## 6η εβδομάδα (6th week) [15-03-2021 - 21-03-2021]

[11η διάλεξη (11th lecture) -video-](https://youtu.be/e1KjXb42fgU)

[11η διάλεξη (11th lecture) -pdf-](../files/11th_lecture.pdf)

[12η διάλεξη (12th lecture) -video-](https://youtu.be/v9ICD8zxBJo)

[12η διάλεξη (12th lecture) -pdf-](../files/12th_lecture.pdf)


## 5η εβδομάδα (5th week) [08-03-2021 - 14-03-2021]
[1st set](https://kesmarag.gitlab.io/courses/mem264/files/1st_set.pdf)

[9η διάλεξη (9th lecture) -video-](https://youtu.be/Oa793XyrJp8)

[9η διάλεξη (9th lecture) -pdf-](../files/9th_lecture.pdf)

[10η διάλεξη (10th lecture) -video-](https://youtu.be/f6SXev7z7ag)

[10η διάλεξη (10th lecture) -pdf-](../files/10th_lecture.pdf)


## 4η εβδομάδα (4th week) [01-03-2021 - 07-03-2021]
[7η διάλεξη (7th lecture) -video-](https://youtu.be/I9FLT30FJF8)

[7η διάλεξη (7th lecture) -pdf-](../files/7th_lecture.pdf)

[8η διάλεξη (8th lecture) -video-](https://youtu.be/iYFe7vce-T8)

[8η διάλεξη (8th lecture) -pdf-](../files/8th_lecture.pdf)


## 3η εβδομάδα (3rd week) [22-02-2021 - 28-02-2021]

[5η διάλεξη (5th lecture) -video-](https://youtu.be/0J83Co7olMk)

[5η διάλεξη (5th lecture) -pdf-](../files/5th_lecture.pdf)

[6η διάλεξη (6th lecture) -video-](https://youtu.be/cwnDBE3MCV8)

[6η διάλεξη (6th lecture) -pdf-](../files/6th_lecture.pdf)

[example.py](../files/example.py)

## 2η εβδομάδα (2nd week) [15-02-2021 - 21-02-2021]

[3η διάλεξη (3rd lecture) -video-](https://youtu.be/hnEeWQA763w)

[3η διάλεξη (3rd lecture) -pdf-](../files/3rd_lecture.pdf)

[4η διάλεξη (4th lecture) -video-](https://youtu.be/C92Izfc-W4E)

[4η διάλεξη (4th lecture) -pdf-](../files/4th_lecture.pdf)

## 1η εβδομάδα (1st week) [08-02-2021 - 14-02-2021]

[1η διάλεξη (1st lecture) -video-](https://youtu.be/0_DG4nrjK9Y)

[1η διάλεξη (1st lecture) -pdf-](../files/1st_lecture.pdf)

[Εργαστήριο 1ης εβδομάδας (βιντεο)](https://youtu.be/_W3sxZXq_F0)

[Εργαστήριο 1ης εβδομάδας (ipynb)](https://colab.research.google.com/drive/1pLWm6Yiqf9eHrvX4wbFrkzxryAlKoGYt?usp=sharing)

[2η διάλεξη (2nd lecture) -video-](https://youtu.be/dCmOjsQth2M)

[2η διάλεξη (2nd lecture) -pdf-](../files/2nd_lecture.pdf)
