from scipy.stats import norm
import numpy as np

np.random.seed(1)

mu1 = 1
mu2 = 1
mu3 = 1
sigma2_1 = 2
sigma2_2 = 2
sigma2_3 = 2

n1 = 2000
n2 = 3000
n3 = 500
I = 3

# Y_1j
Y1 = norm(mu1, sigma2_1**0.5)
# Y_2j
Y2 = norm(mu2, sigma2_2**0.5)
# Y_3j
Y3 = norm(mu3, sigma2_3**0.5)

# samples
y1 = Y1.rvs(n1)
y2 = Y2.rvs(n2)
y3 = Y3.rvs(n3)

# bar y_1
bar_y1 = sum(y1)/n1
# bar y_2
bar_y2 = sum(y2)/n2
# bar y_3
bar_y3 = sum(y3)/n3

# Overall sample mean
bar_y = (sum(y1) + sum(y2) + sum(y3))/(n1+n2+n3)

# s2_1
s2_1 = sum((y1 - bar_y1)**2)/(n1-1) 
# s2_2
s2_2 = sum((y2 - bar_y2)**2)/(n2-1) 
# s2_3
s2_3 = sum((y3 - bar_y3)**2)/(n3-1) 


s2_p = ((n1-1)*s2_1 + (n2-1)*s2_2 + (n3-1)*s2_3)/(n1+n2+n3-3)

s2_b = (n1*(bar_y1 - bar_y)**2 + n2*(bar_y2 - bar_y)**2 + n3*(bar_y3 - bar_y)**2)/(I-1)

print('s2_p =', s2_p)
print('s2_b =', s2_b)

F = s2_b/s2_p

print('F-statistic =', F)
