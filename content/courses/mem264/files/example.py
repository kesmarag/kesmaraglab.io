import numpy as np
import matplotlib.pyplot as plt
from scipy.stats import binom, norm

N = 100
Theta = 0.5 # H_0
Mu = N*Theta
Sigma2 = N*Theta*(1-Theta)

X = binom(N,Theta) # X is a random variable
W = norm(Mu,np.sqrt(Sigma2))

x = range(101) # sample space {0,1,...,100}

plt.plot(x, X.pmf(x), 'o')
plt.plot(x, W.pdf(x))

plt.savefig('./H0_binom_vs_norm.png')

t_obs_binom = 60
p_value_binom = 1 - X.cdf(t_obs_binom - 1)

print('p_value_binom =',p_value_binom)

t_obs_norm = (t_obs_binom - 50)/5

Z = norm(0,1)
p_value_norm = 1 - Z.cdf(t_obs_norm)

print('p_value_norm =',p_value_norm)
