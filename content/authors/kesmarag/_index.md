---
# Display name
name: Costas Smaragdakis

# Username (this should match the folder name)
authors:
- kesmarag

# Is this the primary user of the site?
superuser: true

# Role/position
role: Adjunct lecturer & postdoctoral researcher

# Organizations/Affiliations
organizations:
- name: Department of Mathematics and Applied Mathematics, University of Crete (UOC)
  url: "https://math.uoc.gr"
- name: Postoctoral Reseacher, School of Applied Mathematical and Physical Sciences, National Technical University of Athens (NTUA).
  url: "https://www1.math.ntua.gr"

# Short bio (displayed in user profile at end of posts)
bio: My research focuses on the development of machine learning models with applications in ocean seismology, acoustics and financial mathematics.


# Social/Academic Networking
# For available icons, see: https://sourcethemes.com/academic/docs/widgets/#icons
#   For an email link, use "fas" icon pack, "envelope" icon, and a link in the
#   form "mailto:your-email@example.com" or "#contact" for contact widget.
social:
- icon: envelope
  icon_pack: fas
  link: mailto:kesmarag@twave.xyz
# link: '#contact'  # For a direct email link, use "mailto:kesmarag@uoc.gr".
- icon: gitlab
  icon_pack: fab
  link: https://gitlab.com/kesmarag
- icon: researchgate
  icon_pack: fab
  link: https://www.researchgate.net/profile/Costas_Smaragdakis 
- icon: facebook
  icon_pack: fab
  link: https://facebook.com/kesmarag
# Link to a PDF of your resume/CV from the About widget.
# To enable, copy your resume/CV to `static/files/cv.pdf` and uncomment the lines below.
# - icon: cv
#   icon_pack: ai
#   link: files/cv.pdf

# Enter email to display Gravatar (if Gravatar enabled in Config)
email: ""
  
# Organizational groups that you belong to (for People widget)
#   Set this to `[]` or comment out if you are not using People widget.
user_groups:
- Researchers
- Visitors
---
This webpage is outdated - Please visit https://kesmarag.github.io

I am an adjunct lecturer at the Department of Mathematics and Applied Mathematics, University of Crete and 
a postoctoral reseacher at the School of Applied Mathematical and Physical Sciences, National Technical University of Athens (NTUA).


